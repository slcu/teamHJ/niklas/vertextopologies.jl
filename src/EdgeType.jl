struct Edge{T<:AbstractVertex, T2<:AbstractCell} <: AbstractEdge{T, T2}
    vertex1::T
    vertex2::T
    cell1::T2
    cell2::T2
    function Edge{T, T2}(v1::T, v2::T, c1::T2, c2::T2) where {T<:AbstractVertex, T2<:AbstractCell}
        if v1 === v2
            error("Error in Edge() - you tried to create an edge which starts and stops at the same vertex. This is forbidden.")
        end
        if c1 === c2
            error("Error in Edge() - you tried to create an edge which has the same cell on both sides. This is forbidden.")
        end
        new(v1, v2, c1, c2)
    end
end
Edge(v1::T, v2::T, c1::T2, c2::T2) where {T<:AbstractVertex, T2<:AbstractCell} = Edge{T, T2}(v1, v2, c1, c2)

function Base.iterate(e::Edge, state=1)
    if state == 1
        return (e.vertex1, state+1)
    elseif state == 2
        return (e.vertex2, state+1)
    elseif state > 2
        return nothing
    end
end


Base.size(::Edge) = (2,)
