
vertices(e::AbstractEdge) = [e.vertex1, e.vertex2]
vertices(c::AbstractCell) = unique(Iterators.flatten(edges(c)))
vertices(t::AbstractTissue) = unique(Iterators.flatten(vertices.(cells(t))))

edges(c::AbstractCell) = c.edges
edges(t::AbstractTissue) = unique(Iterators.flatten(edges.(cells(t))))

cells(t::AbstractTissue) = t.cells

concentrations(c::AbstractCell) = c.concentrations

length(e::AbstractEdge) = sqrt(sum((e.vertex1 - e.vertex2)[:].^2))

function area(c::Cell)
    v = vertices(c)
    x_coords = getfield.(v, :x)
    y_coords = getfield.(v, :y)
    return abs(
        sum(x_coords .* y_coords[[2:end; 1]])
        - sum(x_coords[[2:end; 1]] .* y_coords)
        ) / 2
end

function border(src::Cell, dst::Cell)
    shared_edges = edges(src) ∩ edges(dst)
    sum(length.(shared_edges))
end

function border(src::Cell)
    sum(length.(edges(src)))
end

cells(e::AbstractEdge) = (e.cell1, e.cell2)

function neighbours(c::AbstractCell)
    unique(cells.(edges(c)))
end
